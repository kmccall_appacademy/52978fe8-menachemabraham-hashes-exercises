# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  str_lengths = {}
  str.split.each { |word| str_lengths[word] = word.length }
  str_lengths
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.max_by { |_, val| val }.first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each { |item, quantity| older[item] = quantity }
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_counts = Hash.new(0)
  word.each_char { |chr| letter_counts[chr] += 1 }
  letter_counts
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  elements = {}
  arr.each { |el| elements[el] = true }
  elements.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  result = { even: 0, odd: 0 }
  numbers.each { |n| n.even? ? result[:even] += 1 : result[:odd] += 1 }
  result
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  count = Hash.new(0)

  vowels_in_string = string.scan(/[aeiou]/).sort
  vowels_in_string.each { |vowel| count[vowel] += 1 }
  count.max_by { |_, val| val }.first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  students.select { |_, birthday_month| birthday_month > 6 }
          .keys.combination(2).to_a
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  populations = Hash.new(0)
  specimens.each { |specimen| populations[specimen] += 1 }
  populations.sort_by { |_, val| val }

  populations.count**2 * populations.values.first / populations.values.last
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_sign_letters = letter_counts(normal_sign.downcase)
  character_count(vandalized_sign.downcase).each { |ch, val| normal_sign_letters[ch] -= val }
  normal_sign_letters.all? { |_, val| val >= 0 }
end

def character_count(str)
  letter_counts = Hash.new(0)
  str.each_char { |letter| letter_counts[letter] += 1 }
  letter_counts
end
